package com.urbilog.accessibility.adapter

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.urbilog.accessibility.R
import com.urbilog.accessibility.fragment.PageFragment

class ViewPagerAdapter(fm: FragmentManager) : FragmentStatePagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    override fun getCount(): Int = 3

    override fun getItem(i: Int): Fragment {
        val fragment = PageFragment()

        val iconRes: Int = when (i) {
            0 -> R.drawable.ic_android_24px
            1 -> R.drawable.ic_accessibility_new_24px
            else -> R.drawable.ic_language_24px
        }
        fragment.arguments = Bundle().apply {
            putInt(PageFragment.ARG_ICON_RES, iconRes)
        }
        return fragment
    }

    override fun getPageTitle(position: Int): CharSequence {
        return "OBJECT ${(position + 1)}"
    }
}
