package com.urbilog.accessibility.fastitem

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.mikepenz.fastadapter.items.AbstractItem
import com.urbilog.accessibility.R
import com.urbilog.accessibility.model.Travel

class TravelItem : AbstractItem<TravelItem.ViewHolder>() {

    override val layoutRes: Int = R.layout.item_travel
    override val type: Int = R.id.item_travel

    lateinit var travel: Travel

    override fun getViewHolder(v: View): ViewHolder =
        ViewHolder(v)

    override fun bindView(holder: ViewHolder, payloads: List<Any>) {
        super.bindView(holder, payloads)

        val ctx = holder.view.context

        holder.apply {
            textView.text = ctx.getString(travel.titleRes)

            Glide.with(ctx)
                .load(travel.url)
                .into(imageView)
        }

        identifier = travel.id.toLong()
    }

    override fun unbindView(holder: ViewHolder) {
        super.unbindView(holder)

        holder.apply {
            textView.text = null
            imageView.setImageDrawable(null)
        }
    }

    class ViewHolder(var view: View) : RecyclerView.ViewHolder(view) {
        val textView: TextView by lazy {
            view.findViewById<TextView>(R.id.titleTextView)
        }

        val imageView: ImageView by lazy {
            view.findViewById<ImageView>(R.id.bannerImageView)
        }
    }
}