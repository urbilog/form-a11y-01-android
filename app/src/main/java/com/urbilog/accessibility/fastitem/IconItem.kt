package com.urbilog.accessibility.fastitem

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.mikepenz.fastadapter.items.AbstractItem
import com.urbilog.accessibility.R
import com.urbilog.accessibility.model.Icon

class IconItem : AbstractItem<IconItem.ViewHolder>() {

    override val layoutRes: Int = R.layout.item_icon
    override val type: Int = R.id.item_icon

    lateinit var icon: Icon

    override fun getViewHolder(v: View): ViewHolder =
        ViewHolder(v)

    override fun bindView(holder: ViewHolder, payloads: List<Any>) {
        super.bindView(holder, payloads)

        val ctx = holder.view.context

        holder.apply {
            textView.text = ctx.getString(icon.titleRes)
            imageView.setImageDrawable(ContextCompat.getDrawable(ctx, icon.imageRes))
        }

        identifier = icon.id.toLong()
    }

    override fun unbindView(holder: ViewHolder) {
        super.unbindView(holder)

        holder.apply {
            textView.text = null
            imageView.setImageDrawable(null)
        }
    }

    class ViewHolder(var view: View) : RecyclerView.ViewHolder(view) {
        val textView: TextView by lazy {
            view.findViewById<TextView>(R.id.iconTextView)
        }

        val imageView: ImageView by lazy {
            view.findViewById<ImageView>(R.id.iconImageView)
        }
    }
}