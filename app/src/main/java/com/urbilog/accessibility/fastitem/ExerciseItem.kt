package com.urbilog.accessibility.fastitem

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.mikepenz.fastadapter.items.AbstractItem
import com.urbilog.accessibility.R
import com.urbilog.accessibility.model.Exercise

class ExerciseItem : AbstractItem<ExerciseItem.ViewHolder>() {

    override val layoutRes: Int = R.layout.item_exercise
    override val type: Int = R.id.item_exercise

    lateinit var exercise: Exercise

    override fun getViewHolder(v: View): ViewHolder =
        ViewHolder(v)

    override fun bindView(holder: ViewHolder, payloads: List<Any>) {
        super.bindView(holder, payloads)

        val ctx = holder.view.context

        holder.apply {
            titleTextView.text = ctx.getString(exercise.titleRes)
            subtitleTextView.text = ctx.getString(exercise.subtitleRes)
            exerciseImageView.setImageDrawable(ContextCompat.getDrawable(ctx, exercise.imageRes))
        }

        identifier = exercise.id.toLong()
    }

    override fun unbindView(holder: ViewHolder) {
        super.unbindView(holder)

        holder.apply {
            titleTextView.text = null
            subtitleTextView.text = null
            exerciseImageView.setImageDrawable(null)
        }
    }

    class ViewHolder(var view: View) : RecyclerView.ViewHolder(view) {
        val titleTextView: TextView by lazy {
            view.findViewById<TextView>(R.id.titleTextView)
        }

        val subtitleTextView: TextView by lazy {
            view.findViewById<TextView>(R.id.subtitleTextView)
        }

        val exerciseImageView: ImageView by lazy {
            view.findViewById<ImageView>(R.id.exerciseImageView)
        }
    }
}