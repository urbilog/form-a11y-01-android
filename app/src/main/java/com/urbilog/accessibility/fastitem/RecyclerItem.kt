package com.urbilog.accessibility.fastitem

import android.annotation.SuppressLint
import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.adapters.ItemAdapter
import com.mikepenz.fastadapter.items.AbstractItem
import com.urbilog.accessibility.R
import com.urbilog.accessibility.data.Data

class RecyclerItem : AbstractItem<RecyclerItem.ViewHolder>() {
    override val layoutRes: Int = R.layout.item_nested_recycler_view
    override val type: Int = R.id.item_nested_recycler_view

    private val itemAdapter = ItemAdapter<IconItem>()
    private val fastAdapter = FastAdapter.with(itemAdapter)

    override fun getViewHolder(v: View): ViewHolder =
        ViewHolder(v)

    @SuppressLint("SetTextI18n")
    override fun bindView(holder: ViewHolder, payloads: List<Any>) {
        super.bindView(holder, payloads)

        holder.apply {
            titleTextView.text = "Part: $adapterPosition"

            val recyclerItems = mutableListOf<IconItem>()

            Data.icons.forEach {
                recyclerItems.add(IconItem().apply {
                    icon = it
                })
            }

            recyclerView.apply {
                adapter = fastAdapter
                layoutManager = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
            }

            itemAdapter.add(recyclerItems)
        }
    }

    override fun unbindView(holder: ViewHolder) {
        super.unbindView(holder)

        holder.apply {
            itemAdapter.clear()
        }
    }

    class ViewHolder(var view: View) : RecyclerView.ViewHolder(view) {
        val titleTextView: TextView by lazy {
            view.findViewById<TextView>(R.id.titleTextView)
        }

        val recyclerView: RecyclerView by lazy {
            view.findViewById<RecyclerView>(R.id.nestedRecyclerView)
        }
    }
}