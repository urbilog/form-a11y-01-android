package com.urbilog.accessibility.fragment

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.urbilog.accessibility.R
import kotlinx.android.synthetic.main.fragment_page.*

class PageFragment : Fragment(R.layout.fragment_page) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        arguments?.takeIf { it.containsKey(ARG_ICON_RES) }?.apply {
            iconImageView.setImageResource(getInt(ARG_ICON_RES))
        }
    }

    companion object {
        const val ARG_ICON_RES = "icon.res"
    }
}