package com.urbilog.accessibility.fragment

import android.os.Bundle
import android.system.Os.accept
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.urbilog.accessibility.R
import kotlinx.android.synthetic.main.fragment_order.*

class OrderFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_order, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        Glide.with(this)
            .load("https://raw.githubusercontent.com/mikepenz/earthview-wallpapers/develop/thumb/yellowstone-united_states-17.jpg")
            .into(bannerImageView)

        likeButton.setOnCheckedChangeListener { checkBox, isChecked ->
            likeContent(isChecked)
        }

        playButton.setOnClickListener {
            launchPlayer()
        }

        downloadButton.setOnClickListener {
            downloadContent()
        }

        settingsButton.setOnClickListener {
            settings()
        }
    }

    private fun likeContent(checked: Boolean) {
        Toast.makeText(requireContext(), "Like content: $checked", Toast.LENGTH_LONG).show()
    }

    private fun downloadContent() {
        Toast.makeText(requireContext(), "Download page", Toast.LENGTH_LONG).show()
    }

    private fun launchPlayer() {
        Toast.makeText(requireContext(), "Player page", Toast.LENGTH_LONG).show()
    }

    private fun settings() {
        MaterialAlertDialogBuilder(context)
            .setTitle(resources.getString(R.string.dialog_settings))
            .setMessage(resources.getString(R.string.lorem))
            .setNeutralButton(resources.getString(R.string.cancel)) { dialog, which ->
                // Respond to neutral button press
            }
            .setPositiveButton(resources.getString(R.string.ok)) { dialog, which ->
                // Respond to positive button press
            }
            .show()
    }
}