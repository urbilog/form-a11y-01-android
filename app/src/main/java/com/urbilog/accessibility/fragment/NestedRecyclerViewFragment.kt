package com.urbilog.accessibility.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.adapters.ItemAdapter
import com.urbilog.accessibility.R
import com.urbilog.accessibility.fastitem.RecyclerItem
import kotlinx.android.synthetic.main.fragment_nested_recycler_view.*

class NestedRecyclerViewFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_nested_recycler_view, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val itemAdapter = ItemAdapter<RecyclerItem>()
        val fastAdapter = FastAdapter.with(itemAdapter)

        mainRecyclerView.apply {
            adapter = fastAdapter
            layoutManager = LinearLayoutManager(context)
        }

        val recyclerItems = mutableListOf<RecyclerItem>()

        for (i in 1..5) {
            recyclerItems.add(RecyclerItem())
        }

        itemAdapter.add(recyclerItems)
    }
}