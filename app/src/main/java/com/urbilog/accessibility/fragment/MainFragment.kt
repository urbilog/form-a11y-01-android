package com.urbilog.accessibility.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.navigation.NavDirections
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.IAdapter
import com.mikepenz.fastadapter.adapters.ItemAdapter
import com.urbilog.accessibility.R
import com.urbilog.accessibility.data.Data
import com.urbilog.accessibility.fastitem.ExerciseItem
import kotlinx.android.synthetic.main.fragment_main.*

class MainFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        (activity as AppCompatActivity).supportActionBar?.show()

        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val itemAdapter = ItemAdapter<ExerciseItem>()
        val fastAdapter = FastAdapter.with(itemAdapter)

        exercisesRecyclerView.apply {
            adapter = fastAdapter
            layoutManager = LinearLayoutManager(context)
        }

        fastAdapter.onClickListener = { v: View?, _: IAdapter<ExerciseItem>, item: ExerciseItem, _: Int ->
            v?.let {
                findNavController().navigate(getDirection(item.identifier.toInt()))
            }
            false
        }

        val exerciseItems = mutableListOf<ExerciseItem>()

        Data.exercises.forEach { exercise ->
            exerciseItems.add(ExerciseItem().apply {
                this.exercise = exercise
            })
        }

        itemAdapter.add(exerciseItems)
    }

    private fun getDirection(id: Int): NavDirections {
        return when (id) {
            R.id.exercise_order_focus -> MainFragmentDirections.actionMainFragmentToOrderFragment()
            R.id.exercise_feedback -> MainFragmentDirections.actionMainFragmentToFeedbackFragment()
            R.id.exercise_colors -> MainFragmentDirections.actionMainFragmentToColorsFragment()
            R.id.exercise_custom_view -> MainFragmentDirections.actionMainFragmentToCustomViewFragment()
            R.id.exercise_accessibility_action -> MainFragmentDirections.actionMainFragmentToAccessibilityActionFragment()
            R.id.exercise_form -> MainFragmentDirections.actionMainFragmentToFormFragment()
            R.id.exercise_recycler_view -> MainFragmentDirections.actionMainFragmentToNestedRecyclerViewFragment()
            R.id.exercise_web_view -> MainFragmentDirections.actionMainFragmentToWebViewFragment()
            R.id.exercise_view_pager -> MainFragmentDirections.actionMainFragmentToViewPagerFragment()
            R.id.exercise_clickable_span -> MainFragmentDirections.actionMainFragmentToClickableSpanFragment()
            R.id.exercise_player -> MainFragmentDirections.actionMainFragmentToPlayerFragment()
            else -> throw IllegalStateException("Should not happen")
        }
    }
}