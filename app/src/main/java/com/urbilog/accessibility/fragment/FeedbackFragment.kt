package com.urbilog.accessibility.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.IAdapter
import com.mikepenz.fastadapter.adapters.ItemAdapter
import com.urbilog.accessibility.R
import com.urbilog.accessibility.data.Data
import com.urbilog.accessibility.fastitem.TravelItem
import kotlinx.android.synthetic.main.fragment_feedback.*

class FeedbackFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        (activity as AppCompatActivity).supportActionBar?.hide()

        return inflater.inflate(R.layout.fragment_feedback, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val itemAdapter = ItemAdapter<TravelItem>()
        val fastAdapter = FastAdapter.with(itemAdapter)

        travelsRecyclerView.apply {
            adapter = fastAdapter
            layoutManager = LinearLayoutManager(context)
        }

        fastAdapter.onClickListener = { v: View?, _: IAdapter<TravelItem>, item: TravelItem, _: Int ->
            v?.let {
                findNavController().navigate(
                    FeedbackFragmentDirections.actionVoiceFeedbackFragmentToVoiceDetailFragment(
                        item.travel.url,
                        getString(item.travel.titleRes)
                    )
                )
            }
            false
        }

        val travels = mutableListOf<TravelItem>()

        Data.travels.forEach { travel ->
            travels.add(TravelItem().apply {
                this.travel = travel
            })
        }

        itemAdapter.add(travels)
    }
}