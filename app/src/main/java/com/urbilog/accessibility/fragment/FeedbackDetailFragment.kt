package com.urbilog.accessibility.fragment

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.urbilog.accessibility.R
import kotlinx.android.synthetic.main.fragment_feedback_detail.*

class FeedbackDetailFragment : Fragment(R.layout.fragment_feedback_detail) {

    private val args: FeedbackDetailFragmentArgs by navArgs()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        Glide.with(this)
            .load(args.argUrl)
            .into(bannerImageView)

        titleTextView.text = args.argTitle
    }
}