package com.urbilog.accessibility.fragment

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.graphics.ColorUtils
import androidx.fragment.app.Fragment
import com.urbilog.accessibility.R
import kotlinx.android.synthetic.main.fragment_colors.*

class ColorsFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_colors, container, false)
    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val foregroundColorInt = ContextCompat.getColor(requireContext(), R.color.exercise_colors_foreground)
        val backgroundColorInt = ContextCompat.getColor(requireContext(), R.color.exercise_colors_background)

        foregroundView.setBackgroundColor(foregroundColorInt)
        backgroundView.setBackgroundColor(backgroundColorInt)

        val contrast = ColorUtils.calculateContrast(foregroundColorInt, backgroundColorInt)

        contrastTextView.text = "Ratio: $contrast"
    }
}