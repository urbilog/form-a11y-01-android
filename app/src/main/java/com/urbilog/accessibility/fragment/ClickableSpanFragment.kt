package com.urbilog.accessibility.fragment

import android.graphics.Color
import android.os.Bundle
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.urbilog.accessibility.R
import kotlinx.android.synthetic.main.fragment_clickablespan.*


class ClickableSpanFragment : Fragment() {

    private val spans = SpannableStringBuilder("Text with clickable text and link").apply {
        setSpan(object : ClickableSpan() {
            override fun onClick(widget: View) {
                Toast.makeText(requireContext(), "Click !", Toast.LENGTH_LONG).show()
            }
        }, 10, 19, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        setSpan(object : ClickableSpan() {
            override fun onClick(widget: View) {
                Toast.makeText(requireContext(), "Link !", Toast.LENGTH_LONG).show()
            }
        }, 29, 33, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_clickablespan, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        clickableSpanTextView.apply {
            text = spans
            movementMethod = LinkMovementMethod.getInstance()
            highlightColor = Color.TRANSPARENT
        }
    }
}