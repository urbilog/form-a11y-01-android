package com.urbilog.accessibility.model

import androidx.annotation.DrawableRes
import androidx.annotation.IdRes
import androidx.annotation.StringRes

data class Icon(
    @IdRes val id: Int,
    @StringRes val titleRes: Int,
    @DrawableRes val imageRes: Int
)