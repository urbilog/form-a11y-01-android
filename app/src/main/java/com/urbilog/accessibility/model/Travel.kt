package com.urbilog.accessibility.model

import androidx.annotation.IdRes
import androidx.annotation.StringRes

data class Travel(
    @IdRes val id: Int,
    @StringRes val titleRes: Int,
    val url: String
)