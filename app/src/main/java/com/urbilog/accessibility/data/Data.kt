package com.urbilog.accessibility.data

import com.urbilog.accessibility.R
import com.urbilog.accessibility.model.Exercise
import com.urbilog.accessibility.model.Icon
import com.urbilog.accessibility.model.Travel

object Data {

    val exercises: List<Exercise> = listOf(
        Exercise(
            R.id.exercise_order_focus,
            R.string.title_hierarchy_1,
            R.string.subtitle_hierarchy_1,
            R.drawable.ic_view_quilt_24px
        ),
        Exercise(
            R.id.exercise_feedback,
            R.string.title_hierarchy_2,
            R.string.subtitle_hierarchy_2,
            R.drawable.ic_record_voice_over_24px
        ),
        Exercise(R.id.exercise_colors, R.string.title_colors, R.string.subtitle_colors, R.drawable.ic_palette_24px),
        Exercise(
            R.id.exercise_custom_view,
            R.string.title_custom_view,
            R.string.subtitle_custom_view,
            R.drawable.ic_engineering_24px
        ),
        Exercise(
            R.id.exercise_accessibility_action,
            R.string.title_accessibility_action,
            R.string.subtitle_accessibility_action,
            R.drawable.ic_accessibility_new_24px
        ),
        Exercise(
            R.id.exercise_form,
            R.string.title_form,
            R.string.subtitle_form,
            R.drawable.ic_list_alt_24px
        ),
        Exercise(
            R.id.exercise_recycler_view,
            R.string.title_recycler_view,
            R.string.subtitle_recycler_view,
            R.drawable.ic_view_list_24px
        ),
        Exercise(
            R.id.exercise_web_view,
            R.string.title_web_view,
            R.string.subtitle_web_view,
            R.drawable.ic_language_24px
        ),
        Exercise(
            R.id.exercise_view_pager,
            R.string.title_view_pager,
            R.string.subtitle_view_pager,
            R.drawable.ic_view_array_24px
        ),
        Exercise(
            R.id.exercise_clickable_span,
            R.string.title_clickable_span,
            R.string.subtitle_clickable_span,
            R.drawable.ic_insert_link_24px
        ),
        Exercise(
            R.id.exercise_player,
            R.string.title_player,
            R.string.subtitle_player,
            R.drawable.ic_ondemand_video_24px
        )
    )

    val icons: List<Icon> = listOf(
        Icon(R.id.icon_1, R.string.exercise_accessibility, R.drawable.ic_accessibility_new_24px),
        Icon(R.id.icon_2, R.string.exercise_droid, R.drawable.ic_android_24px),
        Icon(R.id.icon_3, R.string.exercise_favorite, R.drawable.ic_favorite_border_24px),
        Icon(R.id.icon_4, R.string.exercise_language, R.drawable.ic_language_24px),
        Icon(R.id.icon_5, R.string.exercise_palette, R.drawable.ic_palette_24px),
        Icon(R.id.icon_6, R.string.exercise_voice, R.drawable.ic_record_voice_over_24px),
        Icon(R.id.icon_7, R.string.exercise_eye, R.drawable.ic_visibility_24px),
        Icon(R.id.icon_8, R.string.exercise_order, R.drawable.ic_view_quilt_24px),
        Icon(R.id.icon_9, R.string.exercise_photo, R.drawable.ic_insert_photo_24px),
        Icon(R.id.icon_10, R.string.exercise_engineer, R.drawable.ic_engineering_24px)
    )

    val travels: List<Travel> = listOf(
        Travel(
            R.id.travel_1,
            R.string.travel_1,
            "https://raw.githubusercontent.com/mikepenz/earthview-wallpapers/develop/thumb/yang_zhuo_yong_cuo,_tibet-china-63.jpg"
        ),
        Travel(
            R.id.travel_2,
            R.string.travel_2,
            "https://raw.githubusercontent.com/mikepenz/earthview-wallpapers/develop/thumb/yellowstone-united_states-17.jpg"
        ),
        Travel(
            R.id.travel_3,
            R.string.travel_3,
            "https://raw.githubusercontent.com/mikepenz/earthview-wallpapers/develop/thumb/victoria-australia-31.jpg"
        ),
        Travel(
            R.id.travel_4,
            R.string.travel_4,
            "https://raw.githubusercontent.com/mikepenz/earthview-wallpapers/develop/thumb/valencia-spain-82.jpg"
        ),
        Travel(
            R.id.travel_5,
            R.string.travel_5,
            "https://raw.githubusercontent.com/mikepenz/earthview-wallpapers/develop/thumb/xigaze,_tibet-china-30.jpg"
        ),
        Travel(
            R.id.travel_6,
            R.string.travel_6,
            "https://raw.githubusercontent.com/mikepenz/earthview-wallpapers/develop/thumb/utah-united_states-96.jpg"
        ),
        Travel(
            R.id.travel_7,
            R.string.travel_7,
            "https://raw.githubusercontent.com/mikepenz/earthview-wallpapers/develop/thumb/umm_al_quwain-united_arab_emirates-13.jpg"
        ),
        Travel(
            R.id.travel_8,
            R.string.travel_8,
            "https://raw.githubusercontent.com/mikepenz/earthview-wallpapers/develop/thumb/texas-united_states-26.jpg"
        ),
        Travel(
            R.id.travel_9,
            R.string.travel_9,
            "https://raw.githubusercontent.com/mikepenz/earthview-wallpapers/develop/thumb/siuslaw_national_forest-united_states-92.jpg"
        )
    )
}